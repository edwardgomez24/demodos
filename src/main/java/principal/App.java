package principal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Hello world!
 *
 */
public class App {
//	Class.forName("com.mysql.jdbc.Driver");
	private static Connection conn= null;
	private static String user = "root";
	private static String password = "manager";
	private static String url = "jdbc:mysql://localhost:3306/apirest";
	
    public static void main( String[] args )
    {
    	try {
	    	Class.forName("com.mysql.jdbc.Driver");  
	    	Connection con=DriverManager.getConnection(url,user,password);
	    	
	    	System.out.println("Conectado exitosamente");
	    	
	    	Statement stmt=con.createStatement();  
	    	ResultSet rs = stmt.executeQuery("select * from usuarios");  
	    	
	    	while(rs.next()){  
	    		System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3)); 
	    	}
	    	con.close();  
    	
    	}catch(Exception e){ 
    		System.out.println(e);    	  
    	} 			
    }
}
